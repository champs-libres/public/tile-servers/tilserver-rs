# Tilserver, a tiny tile server

Tile server serve tiles, tiles are used for web tile map.

This is a POC experimenting a tile server in rust. This tile server serve tiles pre-rendered by renderd (they must be present in the local cache disk).

Missing features:

* headers about tile expiration ;
* handling errors (currently, every error make the threads panic) ;

## Installation

Rustup and Cargo [must be installed](https://www.rust-lang.org/tools/install) on your machine.

## Compiling

```bash
cargo build --release
```

The program will be compiled in a single binary, located at `target/release/tilserver`

## Serving

once compiled:

```bash
# if the pre-rendered metatiles are located in /var/lib/mod_tile:
./tilserver -t /var/lib/mod_tile
```

They will be available at the url scheme `http://localhost:3030/{style}/{z}/{x}/{y}.png`.

The **style** is a subdirectory which must be the same name as the style in url. For instance, if the url is `http://localhost:3030/cyclosm/0/0/0.png`, and the tile directory given as argument is `/var/lib/mod_tile`, the metatile for zoom 0 must be located in `/var/lib/mod_tile/cyclosm/0/0/0/0/0/0.meta`.


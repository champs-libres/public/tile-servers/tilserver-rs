use byteorder::{LittleEndian, ReadBytesExt};
use std::convert::TryFrom;
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, SeekFrom};

/// a struct to store metatile
///
/// this struct is inspired by https://github.com/openstreetmap/mod_tile/blob/e70bfafa20c5d46d9895fa584931e738e8e51fa6/includes/metatile.h#L21
#[derive(Debug)]
pub struct MetaTile {
    counter: u32,
    x: u32,
    y: u32,
    z: u32,
    content: BufReader<File>,
}

/// Store and produce tile
pub struct TileStorage {
    pub tile_dir: String,
}

impl TryFrom<File> for MetaTile {
    type Error = std::io::Error;

    fn try_from(file: File) -> Result<Self, Self::Error> {
        let mut buf = BufReader::new(file);
        // TODO we should check that the first bytes contains `META`
        // we skip the first 4 bytes
        buf.seek(SeekFrom::Start(4))?;
        // next 4 bytes: the counter
        let counter = buf.read_u32::<LittleEndian>()?;
        // next 4 bytes are x, y, z
        let x = buf.read_u32::<LittleEndian>()?;
        let y = buf.read_u32::<LittleEndian>()?;
        let z = buf.read_u32::<LittleEndian>()?;

        Ok(MetaTile {
            counter,
            x,
            y,
            z,
            content: buf,
        })
    }
}

impl MetaTile {
    /// get the tile as image inside a metatile
    pub fn get_tile_by_index(&mut self, tile_index: usize) -> Result<Vec<u8>, std::io::Error> {
        // get a tile entry
        let size: usize;
        let offset: u64;

        // move the cursor of the buffer to the correct place
        // at this place, the buffer encounter a loop of:
        // * 4 bytes for the offset;
        // * 4 bytes for the size;
        let steps = 2 * tile_index * 4;
        self.content.seek(SeekFrom::Current(steps as i64))?;
        // the cursor is at the right place, get the bytes:
        offset = { self.content.read_u32::<LittleEndian>()? } as u64;
        size = { self.content.read_u32::<LittleEndian>()? } as usize;

        let mut result: Vec<u8> = Vec::with_capacity(size);
        self.content.seek(SeekFrom::Start(offset)).unwrap();
        let mut take = self.content.by_ref().take(size as u64);
        take.read_to_end(&mut result)?;

        Ok(result)
    }

    pub fn get_tile_index(&self, x: &u32, y: &u32) -> u32 {
        let diff_y = y % 8;
        let diff_x = x % 8;

        8 * diff_x + diff_y
    }

    pub fn get_tile_by_xy(&mut self, x: &u32, y: &u32) -> Result<Vec<u8>, std::io::Error> {
        let index = self.get_tile_index(x, y) as usize;
        self.get_tile_by_index(index)
    }
}

impl TileStorage {
    fn get_metatile(
        &self,
        style: &String,
        x: u32,
        y: u32,
        z: u32,
    ) -> Result<MetaTile, std::io::Error> {
        let meta_path = Self::xyz_to_meta(x, y, z);
        let path_string = format!(
            "{tile_dir}/{style}/{zoom}/{mt0}/{mt1}/{mt2}/{mt3}/{mt4}.meta",
            tile_dir = self.tile_dir,
            style = style,
            zoom = meta_path[0],
            mt0 = meta_path[1],
            mt1 = meta_path[2],
            mt2 = meta_path[3],
            mt3 = meta_path[4],
            mt4 = meta_path[5]
        );

        let file = File::open(path_string)?;

        MetaTile::try_from(file)
    }

    pub fn get_tile(
        &self,
        style: &String,
        x: u32,
        y: u32,
        z: u32,
    ) -> Result<Vec<u8>, std::io::Error> {
        let mut metatile = self.get_metatile(style, x, y, z)?;

        metatile.get_tile_by_xy(&x, &y)
    }

    /// convert xyz coordinates into a path for metatile
    fn xyz_to_meta(x: u32, y: u32, z: u32) -> [u32; 6] {
        // initialize an array filled with zeros
        let mut path: [u32; 6] = [0; 6];
        // get the lower number multiple of height
        // (this is the same as x - (x % 8)
        let mut x_mut = (x >> 3) << 3;
        let mut y_mut = (y >> 3) << 3;
        path[0] = z;

        for i in (1..=5).rev() {
            path[i] = ((x_mut & 0x0f) << 4) | (y_mut & 0x0f);
            x_mut = x_mut >> 4;
            y_mut = y_mut >> 4;
        }

        path
    }
}

#[cfg(test)]
mod test {
    use super::TileStorage;

    #[test]
    fn metatile_loading() {
        let storage = TileStorage { tile_dir: String::from("./data") };
        let result = storage.get_metatile(&String::from("tile"), 0, 0, 0);

        assert!(result.is_ok());
        let metatile = result.unwrap(); 
        assert_eq!(metatile.x, 0);
        assert_eq!(metatile.y, 0);
        assert_eq!(metatile.z, 0);
    }

    #[test]
    fn tile_loading() {
        let storage = TileStorage { tile_dir: String::from("./data") };
        let tile = storage.get_tile(&String::from("tile"), 4, 2, 3);

        assert!(tile.is_ok());
    }

    #[test]
    fn mutiple_tile_loading_from_same_metatile() {
        let storage = TileStorage { tile_dir: String::from("./data") };
        let result = storage.get_metatile(&String::from("tile"), 4, 2, 3);

        assert!(result.is_ok());
        let mut metatile = result.unwrap();

        let tile_423 = metatile.get_tile_by_xy(&4, &2);
        assert!(tile_423.is_ok());
        let tile_443 = metatile.get_tile_by_xy(&4, &4);
        assert!(tile_443.is_ok());

        assert_ne!(tile_423.unwrap(), tile_443.unwrap());

    }
}

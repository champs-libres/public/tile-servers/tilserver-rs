use actix_web::{get, http::StatusCode, web, App, HttpResponse, HttpServer, Responder};
use clap::Clap;
mod metatile;
use metatile::{TileStorage, MetaTile};


/// options for command line interface
#[derive(Clap, Clone)]
#[clap(
    version = "0.0.1",
    author = "Julien Fastré <julien.fastre@champs-libres.coop"
)]
struct Opts {
    #[clap(short, long, default_value = "/etc/mod_tile")]
    tile_dir: String,
}

/// state for the web application
struct WebAppState {
    tile_dir: String,
    pub tile_storage: TileStorage
}

/// display a metatile into png
#[get("/{style}/{z}/{x}/{y}.png")]
async fn display(
    web::Path((style, z, x, y)): web::Path<(String, u32, u32, u32)>,
    app: web::Data<WebAppState>,
) -> impl Responder {
    let tile = match app.tile_storage.get_tile(&style, x, y, z) {
        Ok(m) => m,
        Err(e) => {
            return HttpResponse::InternalServerError()
                .body(format!("Could not open metatile: {:?}", e))
                ;
        }
    };

    HttpResponse::build(StatusCode::OK)
        .content_type("image/png")
        .body(tile)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let opts: Opts = Opts::parse();

    HttpServer::new(move || {
        App::new()
            .data(WebAppState {
                tile_dir: opts.tile_dir.clone(),
                tile_storage: TileStorage {
                    tile_dir: opts.tile_dir.clone()
                }
            })
        .service(display)
    })
    .bind("0.0.0.0:3030")?
    .run()
    .await
}
